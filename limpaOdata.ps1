﻿<#
.SYNOPSIS
  Executa a remoção de arquivos dentro do diretório Odata do referente ao serviço de integração.

.DESCRIPTION
  Executa o Stop dos serviços necessario para exclui os aqruivos do diretório Odata, e inicia os serviços novamente.

.PARAMETER n/a
    <Brief description of parameter input required. Repeat this attribute if required>

.INPUTS
  n/a

.OUTPUTS
 Log file serão armazenados em  in C:\temp\Log<data>.log

.NOTES
  Version:        1.0
  Author:         ladislaujr@gmail.com
  Creation Date:  08-24-2020
  Purpose/Change: Ciação inicial do Srcript
  
.EXAMPLE
  powershell.exe limpadiretorio.ps1
#>

#---------------------------------------------------------[Initialisations]--------------------------------------------------------

#Set Error Action to Silently Continue
$ErrorActionPreference = "SilentlyContinue"


#----------------------------------------------------------[Declarations]----------------------------------------------------------

#Script Version
$sScriptVersion = "1.0"
#Log File Info
$sLogPath = "C:\Temp"
$date=(Get-Date -format "dd-MM-yyyy_HH-mm-ss")
$sLogName = "Log$date.log"
$sLogFile = Join-Path -Path $sLogPath -ChildPath $sLogName

#-----------------------------------------------------------[Functions]------------------------------------------------------------



Function disparomail{
  Param($assunto, $corpo)
  
  Begin{
     "Iniciando envio de email" | out-file $sLogFile -append
  }
  
  Process{
    Try{
        $CredUser = "usercredential"
        $CredPassword = "************"
        $EmailFrom = "remetente@email.com"
        $EmailTo = "destinatario@email.com"
        $Subject = $assunto
        $Body = $corpo
        $SMTPServer = "servermail.com"
        $SMTPClient = New-Object Net.Mail.SmtpClient($SmtpServer, 25)
        $SMTPClient.Credentials = New-Object System.Net.NetworkCredential($CredUser, $CredPassword);
        $SMTPClient.Send($EmailFrom, $EmailTo, $Subject, $Body)
        }
    
    Catch{
      $ErrorMessage= $_.Exception.Message      
      "Ocorreu erro no Scrip de exclusão de arquivos: $ErrorMessage;"| out-file $sLogFile -append
      Break
    }
  }
  
  End{
    If($?){
      "Email enviado com sucesso!"| out-file $sLogFile -append  
    }
  }
}



#-----------------------------------------------------------[Execution]------------------------------------------------------------


"----Inicio da execução da remoção dos arquivos----" | out-file $sLogFile -append  

try{
    $IntSrvc = Get-Service | where-object {$_.Name -eq "spooler"}
    $CsrSrvc = Get-Service | where-object {$_.Name -eq "winrm"}
    $Diretorio = "Z:\diretorio"
    $Arq1 = Get-ChildItem -Path $Diretorio | Where-Object {!$_.PSIsContainer}
    $Arq2 = Get-ChildItem *.* -Path $Diretorio\bkp
    $Arq3 = Get-ChildItem *.* -Path $Diretorio\error
    $QntArq  = $Arq1.Count
    $QntArq2 = $Arq2.Count
    $QntArq3 = $Arq3.count
    $StsSrvStp = "Stopped" 

    #Verifica se existem arquivos para ser excluidos

    if (($QntArq -and $QntArq2 -and $QntArq3) -eq 0) {
        $ResultadoLimpeza = "Não foram encontrados arquivos para serem excluidos"
        $ResultadoSrvStart = "Nome: " + $IntSrvc.Name + " Status: " + $IntSrvc.Status + """`r`n""" +  "Nome: " + $CsrSrvc.Name + " Status: " + $CsrSrvc.Status
    }else{
    #Se encontrar arquivos executa as açoes de limpeza.

    #Parando os serviços
        Stop-Service $IntSrvc
        Stop-Service $CsrSrvc
       
    #valida se os serviços pararam para executar a limpeza, caso não tenha sido parados, aguarda até 10 segundos.
        for ($i=0; $i -lt 10; $i++ ){
     
            $ChkSrv1 = Get-Service $IntSrvc.Name
            $ChkSrv2 = Get-Service $CsrSrvc.Name
            #write-host = $ChkSrv1.Status "/" $ChkSrv2.Status
       
            #Se os serviços foram parados, segue a execução
            if ($ChkSrv2.Status -and $ChkSrv1.Status -eq $StsSrvStp){   
            #write-host $ChkSrv1 " / " $ChkSrv2             
    
                #Removendo arquivos listados nos 3 diretórios.
                $Arq1 | foreach {remove-Item $_.FullName}        
                $Arq2 | foreach {remove-Item $_.FullName}        
                $Arq3 | foreach {remove-Item $_.FullName}
                
                #Grava quantidade de arquivos encontrados em cada direório que foram excluidos.
                $ExclQnt  = $QntArq  
                $ExclQnt2 = $QntArq2 
                $ExclQnt3 = $QntArq3  

                #Atualizando variaveis com status do diretorio após processo de excluiros arquivos.
                $Arq1 = Get-ChildItem -Path $Diretorio | Where-Object {!$_.PSIsContainer}
                $Arq2 = Get-ChildItem *.* -Path $Diretorio\bkp
                $Arq3 = Get-ChildItem *.* -Path $Diretorio\error

                #Verifica se algum arquivo não foi excluido dos diretórios        
                $QntArq  = $Arq1.Count
                $QntArq2 = $Arq2.Count
                $QntArq3 = $Arq3.count
  
                #Resultado após exclusão
                if (($QntArq + $QntArq2 + $QntArq3) -eq 0) {                    
                    $ResultadoLimpeza = "Foram excluidos todos os Arquivos encontrados: " +"""`r`n"""+ $ExclQnt + " arquivos no diretório Odata, " +"""`r`n"""+ $ExclQnt2 +" arquivos no diretório bkp, e "+"""`r`n""" + $ExclQnt3 + " arquivos no diretório error."   
                }else {
                    $ResultadoLimpeza = "Sobraram " + """`r`n""" + $QntArq + " arquivos no diretório Odata " +"""`r`n""" + $QntArq2 +" no diretório bkp,  " + """`r`n""" + $QntArq3 + " no diretório error."    
                }
                $i=10
             
            }else{
            sleep -Milliseconds 1000      
            $ResultadoLimpeza = "Não foi executado a limpeza pois os serviços não estavam parados."         
            }
        }
            
        #Iniciando serviços após limpeza
        if ($testService.Status -or $CsrSrvc.Status -eq $StsSrvStp){
            Start-Service $IntSrvc
            Start-Service $CsrSrvc       
            $ResultadoSrvStart = "Serviços Iniciados: " + """`r`n""" + $IntSrvc.Name +" Status: " + $IntSrvc.Status + """`r`n""" + $CsrSrvc.Name +" Status: "+ $CsrSrvc.Status
        } else {
            $ResultadoSrvStart = "Status de serviços não alterados: " + """`r`n""" + $IntSrvc.Name +" Status: " + $IntSrvc.Status + """`r`n""" + $CsrSrvc.Name +" Status: "+ $CsrSrvc.Status
        }
    }
    $LogMessage =  "----Status dos serviços----" + """`r`n""" + $ResultadoSrvStart + """`r`n""" + "----Status dos serviços----" + """`r`n""" + "----Status dos Limpeza----" + """`r`n""" + $ResultadoLimpeza + """`r`n""" + "----Status dos Limpeza----"  
    $LogMessage | out-file $sLogFile -append  
    #Chama a function para enviar o resultado por emal
    #disparomail $ResultadoLimpeza, $ResultadoSrvStart 
}catch{    
    $ErrorMessage= $_.Exception.Message      
    "Ocorreu erro no Script de exclusão de arquivos: $ErrorMessage;"| out-file $sLogFile -append
    Break   
   
  
}
"----Final da execução----" | out-file $sLogFile -append

